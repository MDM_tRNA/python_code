
from Bio import SeqIO
from Bio import pairwise2
from operator import eq
from random import choice
import numpy as np


def read_seq_fasta(ref_file, query_file):
    '''
    Read sequences (reference and read) from FASTA files
    '''
    
    with open(ref_file) as fasta_file:  # will close handle cleanly
        ref_len = []
        ref_seq = []
        for seq_record in SeqIO.parse(fasta_file, 'fasta'):  # generator
            ref_len.append(len(seq_record.seq))
            ref_seq.append(seq_record.seq)
            #ref_seq = random_sequence(ref_seq)
    with open(query_file) as fasta_file:  # will close handle cleanly
        que_len = []
        que_ch = []
        que_seq = []
        for seq_record in SeqIO.parse(fasta_file, 'fasta'):  # generator
            que_len.append(len(seq_record.seq))
            ch_loc = seq_record.description.find('ch=')
            que_ch.append(int(''.join(
                    filter(str.isdigit,
                           seq_record.description[ch_loc+3:ch_loc+6]))))
            que_seq.append(seq_record.seq)
            
    return ref_len, ref_seq, que_len, que_seq, que_ch


def align_single_ref(ref_seq, que_seq):
    '''
    Align every read in the query file with the reference, keep only
    one alignment
    '''
        
    # Perform alignment for all reads
    align_res, align_score = [], []
    for i in range(0, len(que_seq)):
        alignment = pairwise2.align.globalms(
                que_seq[i], ref_seq, 3, -2, -2, -.5,
                one_alignment_only=True)
        align_score.append(alignment[0][2])
        align_res.append(alignment)
        
    return align_res, align_score


def eval_error(align_data, ref_len):
    '''
    Evaluate errors of all reads in file
    '''
    
    data_len = len(align_data)
    err_dat = np.zeros((data_len, 3, ref_len))
    err_dat_ins = np.zeros((data_len, ref_len+1))
    for n in range(0, data_len):
        align_ref = align_data[n][0][1]
        align_que = align_data[n][0][0]
        err_dat[n, :, :], err_dat_ins[n, :] = spec_error(
                align_ref, align_que, ref_len)
    
    return err_dat, err_dat_ins, data_len


def spec_error(ref, que, ref_len):
    '''
    Error specifier function
    '''
    
    match = np.array(list(map(eq, ref, que)))
    delet = np.array([x == '-' for x in que])
    insert = np.array([x == '-' for x in ref])
    substit = np.ones(len(match))-match-delet-insert
    # Count insertions (blocks)
    insert_flip = 1 - insert
    insert_len = np.diff(np.nonzero(np.concatenate(([insert[0]],
                                     insert[:-1] != insert[1:],
                                     [True])))[0])[::2]
    insert_gap = np.diff(np.nonzero(np.concatenate(([insert_flip[0]],
                                     insert_flip[:-1] != insert_flip[1:],
                                     [False])))[0])[::2]
    ref_sum = np.cumsum(insert_gap)
    insert_res = np.zeros(ref_len+1)
    if insert[0] == 1:
        ref_sum = np.concatenate(([0], ref_sum))
    insert_res[ref_sum.astype(int)] = insert_len
    
    return np.vstack((match[(insert==0)], delet[(insert==0)],
                       substit[(insert==0)])), insert_res
                            
                            
def random_sequence(sequence):
    '''
    Random sequence generator
    '''
    
    length = len(sequence)
    rand_seq = ''
    for count in range(length):
        rand_seq += choice(sequence)
        
    return rand_seq
                            


