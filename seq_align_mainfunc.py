
import numpy as np
import pandas as pd
import seq_align_subfunc as al_sub
import seq_align_plotfunc as al_plt


def channel_score_disp(ref_file, reads_file):
    '''
    Display number of reads with corresponding score for each channel
    '''
    
    # Read data from FASTA file
    ref_len, ref_seq, que_len, que_seq, que_ch = al_sub.read_seq_fasta(
            ref_file, reads_file)
    # Perform global alignment
    align_res, align_score = al_sub.align_single_ref(ref_seq[0], que_seq)
    # Plot data
    al_plt.plot_score_channel(align_score, que_ch)
        
    
def error_single_ref_disp(ref_file, reads_file, rand_ref = False, 
                          error_dist_seq=True, error_dist_sum=False):
    '''
    Display alignment errors - One reference / multiple reads
    '''
    
    # Read data from FASTA file
    ref_len, ref_seq, que_len, que_seq, que_ch = al_sub.read_seq_fasta(
            ref_file, reads_file)
    # Perform global alignment
    if rand_ref == True:
        ref_seq = [al_sub.random_sequence(ref_seq[0])]
    align_res, align_score = al_sub.align_single_ref(ref_seq[0], que_seq)
    # Evaluate alignments and count errors
    errors, errors_insert, data_len = al_sub.eval_error(align_res, ref_len[0])
    
    # Show error distribution along reference sequence
    if error_dist_seq == True:
        err_base = np.sum(errors, axis=0)/data_len
        err_base_ins = np.sum(errors_insert, axis=0)/data_len
        al_plt.plot_error_base(err_base, err_base_ins, data_len, ref_len[0])
        
    # Show error distribution of all reads (sum over sequence)
    if error_dist_sum == True:
        err_seq = np.sum(errors, axis=2)
        err_seq_ins = np.sum(errors_insert, axis=1)
        al_plt.plot_error_seq(err_seq, err_seq_ins, data_len)
        
    
def align_random_mult(ref_file, reads_file, n_rand=1000):
    '''
    Align reads with multiple references and generated random sequences
    '''
    
    # Read data from FASTA file
    ref_len, ref_seq, que_len, que_seq, que_ch = al_sub.read_seq_fasta(
            ref_file, reads_file)
    
    len_que_file = len(que_seq)
    len_ref_file = len(ref_seq)
    score_array = np.zeros((len_que_file, 2*len_ref_file+4))
    score_all = np.zeros((len_que_file, len_ref_file*n_rand+len_ref_file))
    score_temp = np.zeros((n_rand+1, len_que_file))
    col = ['']*(2*len_ref_file+4)
    for i in range(0, len_ref_file):
        all_seq = [ref_seq[i]]
        for j in range(0, n_rand):
            seq = al_sub.random_sequence(ref_seq[i])
            all_seq.append(seq)
        
        # Perform global alignment
        for k in range(0, n_rand+1):
            align_res, score_temp[k, :] = al_sub.align_single_ref(
                                          all_seq[k], que_seq)
        
        score_trp = np.transpose(score_temp)
        score_all[:, i*n_rand+i:(i+1)*n_rand+(i+1)] = score_trp
        score_ref = np.reshape(score_trp[:, 0], (len_que_file, 1))
        score_array[:, 2*i] = score_trp[:, 0]
        score_array[:, 2*i+1] = np.sum(score_trp[:, 1:]>score_ref, axis=1)
        col[2*i] = 'score align ref ' + str(i+1)
        col[2*i+1] = 'n align rand ref ' + str(i+1) + ' > ref ' + str(i+1)
        
    max_ref_score = np.amax(score_array[:, 0:-4:2], axis=1)
    score_array[:, -4] = max_ref_score
    idx_max = np.argmax(score_array[:, 0:-4:2], axis=1)
    score_array[:, -3] = idx_max + 1
    score_array[:, -2] = np.sum(score_all>np.reshape(
                         max_ref_score, (len_que_file, 1)), axis=1)/(
                         len_ref_file*n_rand+(len_ref_file-1))
    max_score_temp = score_array[:, 0:-4:2]
    score_array[:, -1] = max_ref_score - np.amax(max_score_temp*(
                         max_score_temp!=np.reshape(
                         max_ref_score, (len_que_file, 1))), axis=1)
    col[-4] = 'max align score ref'
    col[-3] = 'ref'
    col[-2] = 'p-value'
    col[-1] = 'diff max score - next best ref'
    
    # Create pandas dataframe from data
    score_df = pd.DataFrame(score_array, columns=col)
    # Write dataframe to csv file
    score_df.to_csv('data_alignment.csv', sep='\t', encoding='utf-8')
       
    return score_df
        
    
    
    
    
     