
import seq_align_mainfunc

ref_file = 'trna1_ref.fa'
reads_file = 'reads_trna1_pass.fa'

if __name__ == "__main__":

    import importlib
    importlib.reload(seq_align_mainfunc)
    
    # Compute score per channel
    seq_align_mainfunc.channel_score_disp(ref_file, reads_file)
    
    # Classification
    #b = seq_align_mainfunc.align_random_mult(ref_file, reads_file, n_rand=100)
    
    # Matches / errors along average read
    seq_align_mainfunc.error_single_ref_disp(ref_file, reads_file,
                                             rand_ref = False, 
                                             error_dist_seq=True, 
                                             error_dist_sum=False)

