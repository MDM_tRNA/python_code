
import numpy as np
import matplotlib.pyplot as plt


def plot_error_base(err_base, err_base_ins, data_len, ref_len):
    '''
    Plotting function - errors along sequence (per base)
    '''
    
    max_err = np.amax(err_base)
    max_err_insert = np.amax(err_base_ins)
    x_err = np.arange(1, ref_len+1)
    x_err_insert = np.arange(0, ref_len+1)
    fig = plt.figure(figsize=(11, 6))
    # Match
    ax1 = fig.add_subplot(2, 2, 1)
    ax1.bar(x_err, err_base[0, :], color='green', width=1, alpha=0.85)
    ax1.set_ylim([0, max_err + 0.1*max_err])
    ax1.set_xlabel(r'$position\ in\ reference\ sequence$')
    ax1.set_ylabel(r"$probability\ of\ 'match'$")
    ax1.set_title('$Matches\ \ (reads\ n_{total}\ =\ ' + str(data_len) + ')$')
    ax1.grid(True)
    # Deletion
    ax2 = fig.add_subplot(2, 2, 2)
    ax2.bar(x_err, err_base[1, :], color='yellow', width=1, alpha=0.85)
    ax2.set_ylim([0, max_err + 0.1*max_err])
    ax2.set_xlabel(r'$position\ in\ reference\ sequence$')
    ax2.set_ylabel(r"$probability\ of\ 'deletion'$")
    ax2.set_title(r'$Deletions\ \ (reads\ n_{total}\ =\ ' + str(data_len) + ')$')
    ax2.grid(True)
    # Substitution
    ax3 = fig.add_subplot(2, 2, 3)
    ax3.bar(x_err, err_base[2, :], color='red', width=1, alpha=0.85)
    ax3.set_ylim([0, max_err + 0.1*max_err])
    ax3.set_xlabel(r'$position\ in\ reference\ sequence$')
    ax3.set_ylabel(r"$probability\ of\ 'substitution'$")
    ax3.set_title(
            r'$Substitutions\ \ (reads\ n_{total}\ =\ ' + str(data_len) + ')$')
    ax3.grid(True)
    # Insertion
    ax4 = fig.add_subplot(2, 2, 4)
    ax4.bar(x_err_insert, err_base_ins, color='mediumblue',
            width=1, alpha=0.85, align='edge')
    ax4.set_ylim([0, max_err_insert + 0.1*max_err_insert])
    ax4.set_xlabel(r'$position\ in\ reference\ sequence$')
    ax4.set_ylabel(r"$insertions\ (average)$")
    ax4.set_title(
            r'$Insertions\ \ (reads\ n_{total}\ =\ ' + str(data_len) + ')$')
    ax4.grid(True)
    # Use tight layout
    plt.tight_layout()
    plt.savefig('dist_along_seq.pdf', bbox_inches='tight')
    plt.savefig('dist_along_seq.png', bbox_inches='tight')
    plt.show()
    
    fig = plt.figure(figsize=(8, 5))
    # Match
    #ax1 = fig.add_subplot(2, 1, 1)
    ax1 = plt.Axes(fig, [.1, .32, .85, .60])
    fig.add_axes(ax1)
    ax1.bar(x_err, err_base[0, :], color='green', width=1, alpha=0.85)
    ax1.set_ylim([0, max_err + 0.1*max_err])
    ax1.set_xlabel(r'$position\ in\ reference\ sequence$')
    ax1.set_ylabel(r"$probability\ of\ 'match'$")
    ax1.set_title(
            "$Probability\ of\ 'match'\ compared\ to\ position\ of\ bonds$")
    ax1.grid(True)
    bonds = np.array([1,1,1,1,1,0,1,0,0,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,0,1,1,
                      1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,
                      0,0,0,0,1,1,1,1,1,1,0,1,1,1,1,1,0,0,0,0])
    #ax2 = fig.add_subplot(2, 1, 2)
    ax2 = plt.Axes(fig, [.1, .1, .85, .10])
    fig.add_axes(ax2)
    bd = ax2.bar(x_err, bonds, color='red', width=0.4, alpha=1)
    for x in [2, 12, 21, 27, 41, 48, 64, 69]:
        bd[x].set_color('b')
    ax2.set_ylim([0.0, 1.0])
    ax2.set_yticks([0, 1])
    ax2.set_ylabel(r"$bonds$")
    # Save figure
    plt.savefig('match_comp_bonds.pdf', bbox_inches='tight')
    plt.savefig('match_comp_bonds.png', bbox_inches='tight')
    plt.show()
    
    
def plot_error_seq(err_seq, err_seq_ins, data_len):
    '''
    Plotting function - errors over all sequences
    '''
    
    fig = plt.figure(figsize=(11, 6))
    # Match
    ax1 = fig.add_subplot(2, 2, 1)
    ax1.hist(err_seq[:, 0], 50, normed=0, facecolor='green', alpha=0.85)
    ax1.set_xlabel(r'$matches\ in\ aligned\ sequence\ (read)$')
    ax1.set_ylabel(r"$number\ of\ reads$")
    ax1.set_title('$Matches\ \ (reads\ n_{total}\ =\ ' + str(data_len) + ')$')
    ax1.grid(True)
    # Deletion
    ax2 = fig.add_subplot(2, 2, 2)
    ax2.hist(err_seq[:, 1], 50, normed=0, facecolor='yellow', alpha=0.85)
    ax2.set_xlabel(r'$deletions\ in\ aligned\ sequence\ (read)$')
    ax2.set_ylabel(r"$number\ of\ reads$")
    ax2.set_title('$Deletions\ \ (reads\ n_{total}\ =\ ' + str(data_len) + ')$')
    ax2.grid(True)
    # Substitution
    ax3 = fig.add_subplot(2, 2, 3)
    ax3.hist(err_seq[:, 2], 50, normed=0, facecolor='red', alpha=0.85)
    ax3.set_xlabel(r'$substitutions\ in\ aligned\ sequence\ (read)$')
    ax3.set_ylabel(r"$number\ of\ reads$")
    ax3.set_title(
            '$Substitutions\ \ (reads\ n_{total}\ =\ ' + str(data_len) + ')$')
    ax3.grid(True)
    # Insertion
    ax4 = fig.add_subplot(2, 2, 4)
    ax4.hist(err_seq_ins, 50, normed=0, facecolor='mediumblue', alpha=0.85)
    ax4.set_xlabel(r'$insertions\ in\ aligned\ sequence\ (read)$')
    ax4.set_ylabel(r"$number\ of\ reads$")
    ax4.set_title('$Insertions\ \ (reads\ n_{total}\ =\ ' + str(data_len) + ')$')
    ax4.grid(True)
    # Use tight layout
    plt.tight_layout()
    plt.savefig('dist_over_all_seq.pdf', bbox_inches='tight')
    plt.show()
    
    
def plot_score_channel(align_score, channel_spec):
    '''
    Evaluate and plot score / channel dependence
    '''
    
    sc = np.array(align_score)
    ch = np.array(channel_spec)
    y_max = np.amax(sc)
    y_min = np.amin(sc)
    y_range = y_max - y_min
    y_max_lim = np.around(y_max + 0.12*y_range, -1)
    y_min_lim = np.around(y_min - 0.12*y_range, -1)
    
    fig = plt.figure(figsize=(12, 6))
    ax1 = fig.add_subplot(4, 1, 1)
    ax1.scatter(ch[(ch <= 128)], sc[(ch <= 128)], s=10,
                   c='forestgreen', alpha=0.7)
    ax1.set_xlim([0, 129])
    ax1.set_ylim([y_min_lim, y_max_lim])
    ax1.set_xticks(np.arange(0, 130, 4))
    ax1.set_yticks([y_min_lim, y_max_lim])
    ax1.set_ylabel(r'$score$')
    ax1.set_title(r'$Reads\ (alignments)\ per\ channel:\ ' +
                  'Alignment\ score\ (Needleman-Wunsch\ algorithm)$')
    ax1.grid(True)
    ax2 = fig.add_subplot(4, 1, 2)
    ax2.scatter(ch[(128 < ch) & (ch <= 256)], sc[(128 < ch) & (ch <= 256)],
                   s=10, c='forestgreen', alpha=0.7)
    ax2.set_xlim([128, 257])
    ax2.set_ylim([y_min_lim, y_max_lim])
    ax2.set_xticks(np.arange(128, 258, 4))
    ax2.set_yticks([y_min_lim, y_max_lim])
    ax2.set_ylabel(r'$score$')
    ax2.grid(True)
    ax3 = fig.add_subplot(4, 1, 3)
    ax3.scatter(ch[(256 < ch) & (ch <= 384)], sc[(256 < ch) & (ch <= 384)],
                   s=10, c='forestgreen', alpha=0.7)
    ax3.set_xlim([256, 385])
    ax3.set_ylim([y_min_lim, y_max_lim])
    ax3.set_xticks(np.arange(256, 386, 4))
    ax3.set_yticks([y_min_lim, y_max_lim])
    ax3.set_ylabel(r'$score$')
    ax3.grid(True)
    ax4 = fig.add_subplot(4, 1, 4)
    ax4.scatter(ch[(384 < ch) & (ch <= 512)], sc[(384 < ch) & (ch <= 512)],
                   s=10, c='forestgreen', alpha=0.7)
    ax4.set_xlim([384, 513])
    ax4.set_ylim([y_min_lim, y_max_lim])
    ax4.set_xticks(np.arange(384, 514, 4))
    ax4.set_yticks([y_min_lim, y_max_lim])
    ax4.set_xlabel(r'$channel$')
    ax4.set_ylabel(r'$score$')
    ax4.grid(True)
    plt.tight_layout()
    plt.savefig('score_per_channel.pdf', bbox_inches='tight')
    plt.show()
    
    